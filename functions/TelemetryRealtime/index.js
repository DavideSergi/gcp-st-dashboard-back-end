const Datastore = require('@google-cloud/datastore');
const admin = require('firebase-admin');

const registryId = "gcp-dashboard"
const projectId = "stm32iot-206219"
const cloudRegion = "europe-west1"
const databaseURL = 'https://stm32iot-206219.firebaseio.com'
const kind = "UserDeviceRegistration"
// Creates a client
const datastore = new Datastore({
    projectId: projectId,
});
// Initialize Admin Firebase handler
admin.initializeApp({
    credential: admin.credential.applicationDefault(),
    databaseURL: databaseURL,
    projectId:projectId

});

exports.TelemetryRealtime = (event, callback) => {
    
    // Message normalized payload to just forward to FCM infrastructure
    const pubsubmessage = event.data;
    
    var message = pubsubmessage.data;
    message = Buffer.from(message, 'base64').toString();
    var parsedmessage = JSON.parse(message)

    var deviceId = parsedmessage["id"];    
    
    console.log("(Decoded)Event received:");
    console.log(parsedmessage);

    var query = null;
    query = datastore
        .createQuery(kind)
        .filter('deviceId','=',deviceId)

    /* 1- Retrieve all registrations for device, to retrieve all groups which it follows */
    datastore
        .runQuery(query)
        .then(
            result => {
                var registrations = result[0];
                var registrationsCount = registrations.length;
                
                if(registrations.length===0)
                {
                    console.log("No forwarding operations are necessary (no registrations for device "+deviceId+" )");
                    callback();
                }

                for(var index in registrations){
                    var registration = registrations[index];
                    var topicName = registration.userId;

                    // See documentation on defining a message payload.
                    var messageForward = {
                        "data":{
                        //    "title": "mess",
                            "message": JSON.stringify(parsedmessage)
                        } ,
                        // topic: topicName
                    };

                    // Send a message to devices subscribed to the provided topic.
                    admin.messaging().sendToTopic(topicName,messageForward)
                        .then((response) => {
                            console.log("Success. Message forwarded to '"+topicName+"' topic");
                            console.log(response);

                            registrationsCount--;
                            if(registrationsCount===0){
                                console.log("Forwarding terminated with success");
                                callback();
                            }
                        })
                        .catch((err) => {
                            console.log("Something gone wrong during forwarding:");
                            console.log(err);
                            callback();
                        });
                }
            }
        ).catch(
            result =>{
                console.log("Something gone wrong during reading from database");
                callback();
            }
        );

    // For pushing message in FCM architecture we need to send:
    // POST https://fcm.googleapis.com/v1/projects/myproject-b5ae1/messages:send HTTP/1.1
    // Content-Type: application/json
    // Authorization: Bearer ya29.ElqKBGN2Ri_Uz...HnS_uNreA [if application -this case- run in a trusted enviroment is necessary an authN token]
    // {
    //     "message":{
    //         "topic" : "foo-bar",
    //         "notification" : {
    //             "body" : "This is a Firebase Cloud Messaging Topic Message!",
    //             "title" : "FCM Message"
    //         }
    //     }
    // }
    // axios({
    //     method:'post',
    //     url:'https://fcm.googleapis.com/v1/projects/stm32iot-206219/messages:send',
    //     contentType:'application/json',
    //     headers: {
    //         'Content-Type': 'application/json',
    //         'Authorization':'Bearer {}'
    //     },
    //     data:{
    //         "message":{
    //             "topic": deviceId, // Group id
    //             "notification": {
    //                 "title": "telemetry",
    //                 "body": message
    //             }
    //         }
    //     }
    // })
    // .then(
    //     function (response) {
    //         console.log("Success forward: ");
    //         console.log(response);
    //         callback();
    //     }
    // ).catch(
    //     function(err){
    //         console.log("Something gone wrong:");
    //         console.log(err);
    //         callback();
    //     }
    // );
}
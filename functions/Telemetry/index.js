// const axios = require(axios);
const cors = require('cors')({origin: true});
const admin = require('firebase-admin');
const express = require('express');
const app = express();

// Imports the Google Cloud client library
const Datastore = require('@google-cloud/datastore');
// Your server api key
const serverApiKey = 'AIzaSyCIbfpaLZKaGWDcAeKDOQyALWlJBgly2wU'
// Your Google Cloud Platform project ID
const projectId = 'stm32iot-206219';
// The kind for the new entity
const kind = 'Telemetry';

// Express middleware that validates Firebase ID Tokens passed in the Authorization HTTP header.
// The Firebase ID token needs to be passed as a Bearer token in the Authorization HTTP header like this:
// `Authorization: Bearer <Firebase ID Token>`.
// when decoded successfully, the ID Token content will be added as `req.user`.
const validateFirebaseIdToken = (req, res, next) => {

    //Doesn't require Authorization header for CORS handshaking
    if(req.method==='OPTIONS'){
        return next();
    }

    console.log('Check if request is authorized with Firebase ID token');
  
    if ((!req.headers.authorization || !req.headers.authorization.startsWith('Bearer '))) {
      console.error('No Firebase ID token was passed as a Bearer token in the Authorization header.',
          'Make sure you authorize your request by providing the following HTTP header:',
          'Authorization: Bearer <Firebase ID Token>');
      res.status(403).send('Unauthorized');
      return;
    }
  
    let idToken;
    if (req.headers.authorization && req.headers.authorization.startsWith('Bearer ')) {
      console.log('Found "Authorization" header');
      // Read the ID Token from the Authorization header.
      idToken = req.headers.authorization.split('Bearer ')[1];
    } else {
      res.status(403).send('Unauthorized');
      return;
    }
    admin.auth().verifyIdToken(idToken).then((decodedIdToken) => {
      console.log('ID Token correctly decoded', decodedIdToken);
      req.user = decodedIdToken;
      return next();
    }).catch((error) => {
      console.error('Error while verifying Firebase ID token:', error);
      res.status(403).send('Unauthorized');
    });
  };
// \ End validation middlware

// Initialize Admin Firebase handler
admin.initializeApp({
    credential: admin.credential.applicationDefault(),
    databaseURL: 'https://stm32iot-206219.firebaseio.com',
    projectId:projectId
});


// Check existing device identity
app.get("/mesaures/devices",(req,res)=>{
    /* Local DEBUG CORS handling*/
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "*");
    /* \ Local DEBUG CORS handling*/

    const rawDeviceIds = (req.query.deviceIds)?req.query.deviceIds:[];// deviceIds
    const deviceIds = rawDeviceIds.split(' ');
    
    //const deviceid = (req.query.deviceId)?req.query.deviceId:"";
    const mesaure =  req.query.mesaure;
    const start =  (req.query.start)?req.query.start:"";
    const end =  (req.query.end)?req.query.end:"";
    const count =  (req.query.count)?req.query.count:0;

    // Creates a client
    const datastore = new Datastore({
        projectId: projectId,
    });

    // Telemetry data to return
    allTelemetryData = [];

    var queryCount = deviceIds.length;
    for(var deviceIdIndex in deviceIds){
        var deviceId =  deviceIds[deviceIdIndex];
        // Creates query
        var query = null;

        if(req.query.count!=undefined){
            // Get last N telemetry data with 
            // timestamp >= <given timestamp>
            query = datastore
            .createQuery(kind)
            .filter('id','=',deviceId)
            .filter('timestamp','>=',new Date(start))
            .order('timestamp', {
                descending: true,
            })            
            .limit(count);
        }
        else{
            // Get  telemetry data with 
            // timestamp >= <given timestamp>
            // timestamp <= <given timestamp>
            query = datastore
            .createQuery(kind)
            .filter('id','=',deviceId)
            .filter('timestamp','<=',new Date(end))
            .filter('timestamp','>=',new Date(start));
        }


        // Perform query
        datastore
            .runQuery(query)
            .then(
                result => {
                    datatelemetry = result[0];
                    // datatelemetryresult = [];
                    if(mesaure){

                        // Manually projection
                        for(var index in datatelemetry){
                            var obj = {};
                            // console.log(datatelemetry[index]);
                            obj['id'] = datatelemetry[index].id;
                            obj['timestamp'] = datatelemetry[index].timestamp;
                            obj[mesaure] = datatelemetry[index][mesaure];
                            // console.log(obj);
                            // datatelemetryresult.push(obj);
                            allTelemetryData.push(obj);
                        }
                    }else{
                        //datatelemetryresult = allTelemetryData;
                    }
                    // Check if queries terminated
                    if(queryCount>0)
                        queryCount--;
                    if(queryCount===0)
                        res.status(200).send(allTelemetryData);
                }
            ).catch(
                result =>{
                    res.status(500).send(result);
                }
            );
    }
});


// Check existing device identity
app.get("/mesaures",(req,res)=>{
    /* Local DEBUG CORS handling*/
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "*");
    /* \ Local DEBUG CORS handling*/

    // const rawDeviceIds = (req.query.deviceId)?req.query.deviceId:"";// deviceIds
    // const deviceIds = rawDeviceIds.split('&');

    const deviceId = (req.query.deviceId)?req.query.deviceId:"";
    const mesaure =  req.query.mesaure;
    const start =  (req.query.start)?req.query.start:"";
    const end =  (req.query.end)?req.query.end:"";

    // Creates a client
    const datastore = new Datastore({
        projectId: projectId,
    });

    // Telemetry data to return

    // Creates query
    var query = null;
    query = datastore
            .createQuery(kind)
            .filter('id','=',deviceId)
            .filter('timestamp','<=',new Date(end))
            .filter('timestamp','>=',new Date(start));

    // Perform query
    datastore
            .runQuery(query)
            .then(
                result => {
                    datatelemetry = result[0];
                    datatelemetryresult = [];
                    if(mesaure){
                        // Manually projection
                        for(var index in datatelemetry){
                            var obj = {};
                            // console.log(datatelemetry[index]);
                            obj['id'] = datatelemetry[index].id;
                            obj['timestamp'] = datatelemetry[index].timestamp;
                            obj[mesaure] = datatelemetry[index][mesaure];
                            // console.log(obj);
                            datatelemetryresult.push(obj);
                        }
                    }else{
                        // datatelemetryresult = datatelemetryresult;
                    }
                    // Check if queries terminated
                    res.status(200).send(datatelemetryresult);
                }
            ).catch(
                result =>{
                res.status(500).send(result);
            }
        );
    
});

// Register to listen message caming from devices owned by authenticated user.
app.post("/mesaures/start-listen",(req,res)=>{
    /* Local DEBUG CORS handling*/
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "*");
    /* \ Local DEBUG CORS handling*/
    
    // Topic name
    var userId = req.user.uid;
    var registrationToken = req.body.registrationToken;
    var registrationTokens = [registrationToken];
    // These registration tokens come from the client FCM SDKs.
  
    // Subscribe the devices corresponding to the registration tokens to the
    // topic.
    admin
        .messaging()
        .subscribeToTopic(registrationTokens, userId)
        .then(function(response) {
            console.log("Registration to "+userId+" success.");
            console.log(response);
            res.status(200).send(response);
        })
        .catch(function(err) {
            console.log("Registration to "+userId+" failed.");
            console.log(err);
            res.status(400).send(err);
        });
    // To subscribe a user, targeted by registrationToken, to its topic.
    // METHOD: POST
    // https://iid.googleapis.com/iid/v1/<registration-token>/rel/topics/<topic-name>
    // Content-Type:application/json
    // Authorization:key=<server-api-key>
    // axios({
    //     method:'post',
    //     url:'https://iid.googleapis.com/iid/v1/'+registrationToken+'/rel/topics/'+userId,
    //     contentType:'application/json',
    //     headers: {
    //         'Content-Type': 'application/json',
    //         'Authorization':':key='+serverApiKey
    //     },
    // })
    // .then(
    //     function (response) {
    //         console.log("Registration to "+userId+" success.");
    //         console.log(response);
    //         res.status(200).send(response);
    //     }
    // ).catch(
    //     function(err){
    //         console.log("Registration to "+userId+" failed.");
    //         console.log(err);
    //         res.status(400).send(err);
    //     }
    // );
});
// Unregister to listen message caming from devices owned by authenticated user.
app.post("/mesaures/stop-listen",(req,res)=>{
    /* Local DEBUG CORS handling*/
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "*");
    /* \ Local DEBUG CORS handling*/
    
    // Topic name
    var userId = req.user.uid;
    var registrationToken = req.body.registrationToken;
    var registrationTokens = [registrationToken];
    // These registration tokens come from the client FCM SDKs.
  
    // Subscribe the devices corresponding to the registration tokens to the
    // topic.
    admin
        .messaging()
        .unsubscribeFromTopic(registrationTokens, userId)
        .then(function(response) {
            console.log("Registration to "+userId+" success.");
            console.log(response);
            res.status(200).send(response);
        })
        .catch(function(err) {
            console.log("Registration to "+userId+" failed.");
            console.log(err);
            res.status(400).send(err);
        });
    // To subscribe a user, targeted by registrationToken, to its topic.
    // METHOD: POST
    // https://iid.googleapis.com/iid/v1/<registration-token>/rel/topics/<topic-name>
    // Content-Type:application/json
    // Authorization:key=<server-api-key>
    // axios({
    //     method:'post',
    //     url:'https://iid.googleapis.com/iid/v1/'+registrationToken+'/rel/topics/'+userId,
    //     contentType:'application/json',
    //     headers: {
    //         'Content-Type': 'application/json',
    //         'Authorization':':key='+serverApiKey
    //     },
    // })
    // .then(
    //     function (response) {
    //         console.log("Registration to "+userId+" success.");
    //         console.log(response);
    //         res.status(200).send(response);
    //     }
    // ).catch(
    //     function(err){
    //         console.log("Registration to "+userId+" failed.");
    //         console.log(err);
    //         res.status(400).send(err);
    //     }
    // );
});




// User manager function
//app.use(validateFirebaseIdToken);  // (w/ authN header [ Authorization  |  Bearer <token> ])
app.use(cors);
app.options('*', cors); 
exports.Telemetry = (req, res) => {
    validateFirebaseIdToken(req,res,function(){
        app(req,res);
    }.bind(this))
};

/**
 * Old version: without express
*/
// exports.TelemetryWebApiFunction = (req, res) => {
// 	res.set('Access-Control-Allow-Origin', "*");
//     res.set('Access-Control-Allow-Methods', 'GET');
//     res.set("Access-Control-Allow-Headers", "Content-Type");
//     res.set("Access-Control-Max-Age", "3600");
// 	// CORS handling
//     if(req.method==='OPTIONS'){
//         res.status(200).send();
//         return;
//     }

//     // Log
//     const deviceid = (req.query.deviceId)?req.query.deviceId:"";
//     const mesaure =  req.query.mesaure;
//     const start =  (req.query.start)?req.query.start:"";
//     const end =  (req.query.end)?req.query.end:"";

//     // Creates a client
//     const datastore = new Datastore({
//         projectId: projectId,
//     });
//     // Creates query
//     var query = null;
//     query = datastore
//     .createQuery(kind)
//     .filter('id','=',deviceid)
//     .filter('timestamp','<=',new Date(end))
//     .filter('timestamp','>=',new Date(start));

//     // Perform query
//     datastore
//         .runQuery(query)
//         .then(
//             result => {
//                 datatelemetry = result[0];
//                 datatelemetryresult = [];
//                 if(mesaure){
//                     // Manually projection
//                     for(var index in datatelemetry){
//                         var obj = {};
//                         console.log(datatelemetry[index]);
//                         obj['id'] = datatelemetry[index].id;
//                         obj['timestamp'] = datatelemetry[index].timestamp;
//                         obj[mesaure] = datatelemetry[index][mesaure];
//                         console.log(obj);
//                         datatelemetryresult.push(obj);
//                     }
//                 }else{
//                     datatelemetryresult = datatelemetry;
//                 }
//                 res.status(200).send(datatelemetryresult);
//             }
//         ).catch(
//             result =>{
//                 res.status(400).send(result);
//             }
//         );
// };
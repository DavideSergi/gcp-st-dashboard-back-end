// Imports the Google Cloud client library
const PubSub = require(`@google-cloud/pubsub`);
// Imports the Google Cloud client library
const Datastore = require('@google-cloud/datastore');
// Your Google Cloud Platform project ID
const projectId = 'stm32iot-206219';
// The kind for the new entity
const kind = 'Telemetry';
// The name for the new topic
const topicName = 'gcp-dashboard-events-normalized';
/**
 * Background Cloud Function to be triggered by Pub/Sub.
 * This function is exported by index.js, and executed when
 * the trigger topic receives a message.
 *
 * @param {object} event The Cloud Functions event.
 * @param {function} callback The callback function.
 */
exports.TelemetryStorageFunction = (event, callback) => {
    // Log
    console.log("Trigger event:");
    console.log(event)


    /** Process metadata event **/
    //var metadata = event.textPayload;//JSON.parse(event.textPayload);
    var deviceid = event.data.attributes.deviceId;

    // Twined to device timing alignment
    var timestamp = event.context.timestamp;
    // Decoupling device timing alignment from cloud timing
    //var timestamp = (new Date(Date.now())).toUTCString();
    
    /** Process payload event **/
    const pubsubmessage = event.data;
    var message = pubsubmessage.data;
    message = Buffer.from(message, 'base64').toString();
    if(!message)
    {
        // Message empty/undefined
        callback();
    }

    // Fix JSON telemetry message 
    if(message.charAt(0)!=='{')
        message = "{"+message;
    if(message.charAt(9)!=='{')
        message[8]=' ';
    
    // Parse message
    var parsedmessage = JSON.parse(message)

    var processedmessage = {};
    //processedmessage.timestamp = new Date((new Date(timestamp)).toISOString());
    processedmessage.timestamp = new Date((new Date()).toISOString());
    
    processedmessage.id = deviceid;
    for(var silgledataindex in parsedmessage.data){
        processedmessage[parsedmessage.data[silgledataindex].dataType] = parsedmessage.data[silgledataindex].value;
    }

    // Creates a client
    const datastore = new Datastore({
        projectId: projectId,
    });
    // The name/ID for the new entity
    // The Cloud Datastore key for the new entity
    const taskKey = datastore.key([kind]);
    // Prepares the new entity
    const task = {
        key: taskKey,
        data: processedmessage
    };

    /* 1- Store normalized message in datastore */
    datastore
        .save(task)
        .then(() => {
            console.log(`Saved successfully`);
            
            /* 2- Forward in cloud solution, by pub/sub topic, normalized message just stored*/
            // Instantiates pub/sub client
            const pubsubClient = new PubSub({
                projectId: projectId,
            });

            // Publish message
            pubsubClient
                .topic(topicName)
                .publisher()
                .publish( Buffer.from(JSON.stringify(processedmessage)))
                .then(messageId => {
                    console.log(`Message ${messageId} published.`);
                    callback();
                })
                .catch(err => {
                    console.error('ERROR during message publishing:', err);
                    callback();
                });

        })
        .catch(err => {
            console.error('ERROR during message storaging::', err);
            callback();
        });

};
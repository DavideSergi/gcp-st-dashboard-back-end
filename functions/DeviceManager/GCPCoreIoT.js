/**
 * Util file to comunicate with GCP Core IoT
 */

/**
 * Copyright 2017, Google, Inc.
 * Licensed under the Apache License, Version 2.0 (the `License`);
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an `AS IS` BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

'use strict';
const fs = require('fs');
const {google} = require('googleapis');

const API_VERSION = 'v1';
const DISCOVERY_API = 'https://cloudiot.googleapis.com/$discovery/rest';
module.exports = {
	// Configures the topic for Cloud IoT Core.
	setupIotTopic (topicName) {
	  const PubSub = require('@google-cloud/pubsub');

	  const pubsub = PubSub();
	  const topic = pubsub.topic(topicName);
	  const serviceAccount = `serviceAccount:cloud-iot@system.gserviceaccount.com`;

	  topic.iam.getPolicy()
		.then((results) => {
		  const policy = results[0] || {};
		  policy.bindings || (policy.bindings = []);
		  console.log(JSON.stringify(policy, null, 2));

		  let hasRole = false;
		  let binding = {
			role: 'roles/pubsub.publisher',
			members: [serviceAccount]
		  };

		  policy.bindings.forEach((_binding) => {
			if (_binding.role === binding.role) {
			  binding = _binding;
			  hasRole = true;
			  return false;
			}
		  });

		  if (hasRole) {
			binding.members || (binding.members = []);
			if (binding.members.indexOf(serviceAccount) === -1) {
			  binding.members.push(serviceAccount);
			}
		  } else {
			policy.bindings.push(binding);
		  }

		  // Updates the IAM policy for the topic
		  return topic.iam.setPolicy(policy);
		})
		.then((results) => {
		  const updatedPolicy = results[0];

		  console.log(JSON.stringify(updatedPolicy, null, 2));
		})
		.catch((err) => {
		  console.error('ERROR:', err);
		});
	},

	createIotTopic (topicName) {
	  // Imports the Google Cloud client library
	  const PubSub = require('@google-cloud/pubsub');

	  // Instantiates a client
	  const pubsub = PubSub();

	  pubsub.createTopic(topicName)
		.then((results) => {
		  setupIotTopic(topicName);
		});
	},

	// Lookup the registry, assuming that it exists.
	lookupRegistry (client, registryId, projectId, cloudRegion, cb) {
	  // [START iot_lookup_registry]
	  // Client retrieved in callback
	  // getClient(serviceAccountJson, function(client) {...});
	  // const cloudRegion = 'us-central1';
	  // const projectId = 'adjective-noun-123';
	  // const registryId = 'my-registry';
	  const parentName = `projects/${projectId}/locations/${cloudRegion}`;
	  const registryName = `${parentName}/registries/${registryId}`;
	  const request = {
		name: registryName
	  };

	  client.projects.locations.registries.get(request, (err, res) => {
		if (err) {
		  console.log('Could not look up registry');
		  console.log(err);
		} else {
		  console.log('Looked up existing registry');
		  console.log(res.data);
		}
	  });
	  // [END iot_lookup_registry]
	},

	createRegistry (
	  client,
	  registryId,
	  projectId,
	  cloudRegion,
	  pubsubTopicId,
	  foundCb
	) {
	  // [START iot_create_registry]
	  // Client retrieved in callback
	  // getClient(serviceAccountJson, function(client) {...});
	  // const cloudRegion = 'us-central1';
	  // const projectId = 'adjective-noun-123';
	  // const registryId = 'my-registry';
	  // function errCb = lookupRegistry; // Lookup registry if already exists.
	  const parentName = `projects/${projectId}/locations/${cloudRegion}`;
	  const pubsubTopic = `projects/${projectId}/topics/${pubsubTopicId}`;

	  const request = {
		parent: parentName,
		resource: {
		  eventNotificationConfigs: [{
			'pubsubTopicName': pubsubTopic
		  }],
		  'id': registryId
		}
	  };

	  client.projects.locations.registries.create(request, (err, res) => {
		if (err) {
		  if (err.code === 409) {
			// The registry already exists - look it up instead.
			foundCb(client, registryId, projectId, cloudRegion);
		  } else {
			console.log('Could not create registry');
			console.log(err);
		  }
		} else {
		  console.log('Successfully created registry');
		  console.log(res.data);
		}
	  });
	  // [END iot_create_registry]
	},

	// Create a new registry, or look up an existing one if it doesn't exist.
	lookupOrCreateRegistry (
	  client,
	  registryId,
	  projectId,
	  cloudRegion,
	  pubsubTopicId
	) {
	  // [START iot_lookup_or_create_registry]
	  // Client retrieved in callback
	  // getClient(serviceAccountJson, function(client) {...});
	  // const cloudRegion = 'us-central1';
	  // const projectId = 'adjective-noun-123';
	  // const registryId = 'my-registry';
	  // const pubsubTopicId = 'my-iot-topic';

	  createRegistry(
		client,
		registryId,
		projectId,
		cloudRegion,
		pubsubTopicId,
		lookupRegistry
	  );
	  // [END iot_lookup_or_create_registry]
	},

	// Create a new device with the given id. The body defines the parameters for
	// the device, such as authentication.
	createUnauthDevice (
	  client,
	  deviceId,
	  registryId,
	  projectId,
	  cloudRegion,
	  body
	) {
	  // [START iot_create_unauth_device]
	  // Client retrieved in callback
	  // getClient(serviceAccountJson, function(client) {...});
	  // const cloudRegion = 'us-central1';
	  // const deviceId = 'my-unauth-device';
	  // const projectId = 'adjective-noun-123';
	  // const registryId = 'my-registry';
	  console.log('Creating device:', deviceId);
	  const parentName = `projects/${projectId}/locations/${cloudRegion}`;
	  const registryName = `${parentName}/registries/${registryId}`;

	  const request = {
		parent: registryName,
		resource: {id: deviceId}
	  };

	  client.projects.locations.registries.devices.create(request, (err, res) => {
		if (err) {
		  console.log('Could not create device');
		  console.log(err);
		} else {
		  console.log('Created device');
		  console.log(res.data);
		}
	  });
	  // [END iot_create_unauth_device]
	},

	// Create a device using RSA256 for authentication.
	createRsaDevice (
	  client,
	  deviceId,
	  registryId,
	  projectId,
	  cloudRegion,
		rsaPublicKey,
		metadata,
		successCallback,
		errorCallback
	) {
	  // [START iot_create_rsa_device]
	  // Client retrieved in callback
	  // getClient(serviceAccountJson, function(client) {...});
	  // const cloudRegion = 'us-central1';
	  // const deviceId = 'my-rsa-device';
	  // const projectId = 'adjective-noun-123';
		// const registryId = 'my-registry';
		
	  const parentName = `projects/${projectId}/locations/${cloudRegion}`;
	  const registryName = `${parentName}/registries/${registryId}`;

		const body = {
			id: deviceId,
			credentials: [
				{
					publicKey: {
						format: 'RSA_PEM', //'RSA_X509_PEM',
						key: rsaPublicKey
					}
				}
			],
			metadata:metadata
	  };

	  const request = {
		parent: registryName,
		resource: body
	  };

	  //console.log(JSON.stringify(request));

	  client.projects.locations.registries.devices.create(request, (err, res) => {
			if (err) {
				console.log('Could not create device');
				errorCallback(err);
			} else {
				console.log('Created device');
				successCallback(res);
			}
	  });
	  // [END iot_create_rsa_device]
	},

	// Create a device using ES256 for authentication.
	createEsDevice (
	  client,
	  deviceId,
	  registryId,
	  projectId,
	  cloudRegion,
		esCertificateFile,
		successCallback,
		errorCallback
	) {
	  // [START iot_create_es_device]
	  // Client retrieved in callback
	  // getClient(serviceAccountJson, function(client) {...});
	  // const cloudRegion = 'us-central1';
	  // const deviceId = 'my-es-device';
	  // const projectId = 'adjective-noun-123';
	  // const registryId = 'my-registry';
	  const parentName = `projects/${projectId}/locations/${cloudRegion}`;
	  const registryName = `${parentName}/registries/${registryId}`;
	  const body = {
		id: deviceId,
		credentials: [
		  {
			publicKey: {
			  format: 'ES256_PEM',
			  key: fs.readFileSync(esCertificateFile).toString()
			}
		  }
		]
	  };

	  const request = {
		parent: registryName,
		resource: body
	  };

	  client.projects.locations.registries.devices.create(request, (err, res) => {
		if (err) {
		  console.log('Could not create device');
			console.log(err);
			errorCallback(err);
		} else {
		  console.log('Created device');
			console.log(res.data);
			successCallback(res.data);
		}
	  });
	  // [END iot_create_es_device]
	},

	// Add RSA256 authentication to the given device.
	patchRsa256ForAuth (
	  client,
	  deviceId,
	  registryId,
	  rsaPublicKeyFile,
	  projectId,
	  cloudRegion
	) {
	  // [START iot_patch_rsa]
	  // Client retrieved in callback
	  // getClient(serviceAccountJson, function(client) {...});
	  // const cloudRegion = 'us-central1';
	  // const deviceId = 'my-rsa-device';
	  // const projectId = 'adjective-noun-123';
	  // const registryId = 'my-registry';
	  const parentName =
		  `projects/${projectId}/locations/${cloudRegion}`;
	  const registryName = `${parentName}/registries/${registryId}`;
	  const request = {
		name: `${registryName}/devices/${deviceId}`,
		updateMask: 'credentials',
		resource: {
		  credentials: [
			{
			  publicKey: {
				format: 'RSA_X509_PEM',
				key: fs.readFileSync(rsaPublicKeyFile).toString()
			  }
			}
		  ]
		}
	  };

	  client.projects.locations.registries.devices.patch(request, (err, res) => {
		if (err) {
		  console.log('Error patching device:', deviceId);
		  console.log(err);
		} else {
		  console.log('Patched device:', deviceId);
		  console.log(res.data);
		}
	  });
	  // [END iot_patch_rsa]
	},

	// Add ES256 authentication to the given device.
	patchEs256ForAuth (
	  client,
	  deviceId,
	  registryId,
	  esPublicKeyFile,
	  projectId,
	  cloudRegion
	) {
	  // [START iot_patch_es]
	  // Client retrieved in callback
	  // getClient(serviceAccountJson, function(client) {...});
	  // const cloudRegion = 'us-central1';
	  // const deviceId = 'my-es-device';
	  // const projectId = 'adjective-noun-123';
	  // const registryId = 'my-registry';
	  const parentName =
		  `projects/${projectId}/locations/${cloudRegion}`;
	  const registryName = `${parentName}/registries/${registryId}`;
	  const request = {
		name: `${registryName}/devices/${deviceId}`,
		updateMask: 'credentials',
		resource: {
		  credentials: [
			{
			  publicKey: {
				format: 'ES256_PEM',
				key: fs.readFileSync(esPublicKeyFile).toString()
			  }
			}
		  ]
		}
	  };

	  client.projects.locations.registries.devices.patch(request, (err, res) => {
		if (err) {
		  console.log('Error patching device:', deviceId);
		  console.log(err);
		} else {
		  console.log('Patched device:', deviceId);
		  console.log(res.data);
		}
	  });
	  // [END iot_patch_es]
	},

	// List all of the devices in the given registry.
	listDevices (client, registryId, projectId, cloudRegion) {
	  // [START iot_list_devices]
	  // Client retrieved in callback
	  // getClient(serviceAccountJson, function(client) {...});
	  // const cloudRegion = 'us-central1';
	  // const projectId = 'adjective-noun-123';
	  // const registryId = 'my-registry';
	  const parentName = `projects/${projectId}/locations/${cloudRegion}`;
	  const registryName = `${parentName}/registries/${registryId}`;

	  const request = {
		parent: registryName
	  };

	  client.projects.locations.registries.devices.list(request, (err, res) => {
		if (err) {
		  console.log('Could not list devices');
		  console.log(err);
		} else {
		  let data = res.data;
		  console.log('Current devices in registry:', data['devices']);
		}
	  });
	  // [END iot_list_devices]
	},

	// List all of the registries in the given project.
	listRegistries (client, projectId, cloudRegion) {
	  // [START iot_list_registries]
	  // Client retrieved in callback
	  // getClient(serviceAccountJson, function(client) {...});
	  // const cloudRegion = 'us-central1';
	  // const projectId = 'adjective-noun-123';
	  const parentName = `projects/${projectId}/locations/${cloudRegion}`;

	  const request = {
		parent: parentName
	  };

	  client.projects.locations.registries.list(request, (err, res) => {
		if (err) {
		  console.log('Could not list registries');
		  console.log(err);
		} else {
		  let data = res.data;
		  console.log('Current registries in project:', data['deviceRegistries']);
		}
	  });
	  // [END iot_list_registries]
	},

	// Delete the given device from the registry.
	deleteDevice (
	  client,
	  deviceId,
	  registryId,
	  projectId,
	  cloudRegion,
		success,
		error
	) {
	  // [START iot_delete_device]
	  // Client retrieved in callback
	  // getClient(serviceAccountJson, function(client) {...});
	  // const cloudRegion = 'us-central1';
	  // const projectId = 'adjective-noun-123';
	  // const registryId = 'my-registry';
	  const parentName = `projects/${projectId}/locations/${cloudRegion}`;
	  const registryName = `${parentName}/registries/${registryId}`;
	  const request = {
		name: `${registryName}/devices/${deviceId}`
	  };

	  client.projects.locations.registries.devices.delete(request, (err, res) => {
		if (err) {
		  console.log('Could not delete device:', deviceId);
			console.log(err);
			error()
		} else {
		  console.log('Successfully deleted device:', deviceId);
			console.log(res.data);
			success()

		}
	  });
	  // [END iot_delete_device]
	},

	// Clear the given registry by removing all devices and deleting the registry.
	clearRegistry (client, registryId, projectId, cloudRegion) {
	  const parentName = `projects/${projectId}/locations/${cloudRegion}`;
	  const registryName = `${parentName}/registries/${registryId}`;
	  const requestDelete = {
		name: registryName
	  };

	  const after = function () {
		client.projects.locations.registries.delete(requestDelete, (err, res) => {
		  if (err) {
			console.log('Could not delete registry');
			console.log(err);
		  } else {
			console.log(`Successfully deleted registry ${registryName}`);
			console.log(res.data);
		  }
		});
	  };

	  const request = {
		parent: registryName
	  };

	  client.projects.locations.registries.devices.list(request, (err, res) => {
		if (err) {
		  console.log('Could not list devices');
		  console.log(err);
		} else {
		  let data = res.data;
		  console.log('Current devices in registry:', data['devices']);
		  let devices = data['devices'];
		  if (devices) {
			devices.forEach((device, index) => {
			  console.log(`${device.id} [${index}/${devices.length}] removed`);
			  if (index === devices.length - 1) {
				deleteDevice(
				  client,
				  device.id,
				  registryId,
				  projectId,
				  cloudRegion,
				  after
				);
			  } else {
				deleteDevice(
				  client,
				  device.id,
				  registryId,
				  projectId,
				  cloudRegion
				);
			  }
			});
		  } else {
			after();
		  }
		}
	  });
	},

	// Delete the given registry. Note that this will only succeed if the registry
	// is empty.
	deleteRegistry (client, registryId, projectId, cloudRegion) {
	  // [START iot_delete_registry]
	  // Client retrieved in callback
	  // getClient(serviceAccountJson, function(client) {...});
	  // const cloudRegion = 'us-central1';
	  // const projectId = 'adjective-noun-123';
	  // const registryId = 'my-registry';
	  const parentName = `projects/${projectId}/locations/${cloudRegion}`;
	  const registryName = `${parentName}/registries/${registryId}`;
	  const request = {
		name: registryName
	  };

	  client.projects.locations.registries.delete(request, (err, res) => {
		if (err) {
		  console.log('Could not delete registry');
		  console.log(err);
		} else {
		  console.log('Successfully deleted registry');
		  console.log(res);
		}
	  });
	  // [END iot_delete_registry]
	},

	// Retrieve the given device from the registry.
	getDevice (client, deviceId, registryId, projectId, cloudRegion,successCallback,errorCallback) {
	  // [START iot_get_device]
	  // Client retrieved in callback
	  // getClient(serviceAccountJson, function(client) {...});
	  // const cloudRegion = 'us-central1';
	  // const deviceId = 'my-device';
	  // const projectId = 'adjective-noun-123';
	  // const registryId = 'my-registry';
	  const parentName = `projects/${projectId}/locations/${cloudRegion}`;
	  const registryName = `${parentName}/registries/${registryId}`;
	  const request = {
		name: `${registryName}/devices/${deviceId}`
	  };

	  client.projects.locations.registries.devices.get(request, (err, res) => {
		if (err) {
		  errorCallback(err)
		} else {
		  successCallback(res.data);
		}
	  });
	  // [END iot_get_device]
	},

	// Retrieve the given device's state from the registry.
	getDeviceState (
	  client,
	  deviceId,
	  registryId,
	  projectId,
		cloudRegion,
		successCallback,
		errorCallback
	) {
	  // [START iot_get_device_state]
	  // Client retrieved in callback
	  // getClient(serviceAccountJson, function(client) {...});
	  // const cloudRegion = 'us-central1';
	  // const deviceId = 'my-device';
	  // const projectId = 'adjective-noun-123';
	  // const registryId = 'my-registry';
	  const parentName = `projects/${projectId}/locations/${cloudRegion}`;
	  const registryName = `${parentName}/registries/${registryId}`;
	  const request = {
		name: `${registryName}/devices/${deviceId}`
	  };

	  client.projects.locations.registries.devices.states.list(request,
		(err, data) => {
		  if (err) {
				errorCallback(err);
		  } else {
				successCallback(data.data);
		  }
		});
	  // [END iot_get_device_state]
	},

	// Retrieve the given device's configuration history from the registry.
	getDeviceConfigs (
	  client,
	  deviceId,
	  registryId,
	  projectId,
		cloudRegion,
		successCallback,
		errorCallback
	) {
	  // [START iot_get_device_configs]
	  // Client retrieved in callback
	  // getClient(serviceAccountJson, function(client) {...});
	  // const cloudRegion = 'us-central1';
	  // const deviceId = 'my-device';
	  // const projectId = 'adjective-noun-123';
	  // const registryId = 'my-registry';
	  const parentName = `projects/${projectId}/locations/${cloudRegion}`;
	  const registryName = `${parentName}/registries/${registryId}`;
	  const request = {
		name: `${registryName}/devices/${deviceId}`
	  };

	  client.projects.locations.registries.devices.configVersions.list(request,
		(err, data) => {
		  if (err) {
				errorCallback(err);
		  } else {
				successCallback(data.data);
		  }
		});
	  // [END iot_get_device_configs]
	},


	// Send configuration data to device.
	setDeviceConfig (
	  client,
	  deviceId,
	  registryId,
	  projectId,
	  cloudRegion,
	  data,
	  version,
	  successCallback,
	  errorCallback
	) {
	  // [START iot_set_device_config]
	  // Client retrieved in callback
	  // getClient(serviceAccountJson, function(client) {...});
	  // const cloudRegion = 'us-central1';
	  // const deviceId = 'my-device';
	  // const projectId = 'adjective-noun-123';
	  // const registryId = 'my-registry';
	  // const data = 'test-data';
	  // const version = 0;
	  const parentName = `projects/${projectId}/locations/${cloudRegion}`;
	  const registryName = `${parentName}/registries/${registryId}`;

	  const binaryData = Buffer.from(data).toString('base64');
	  const request = {
		name: `${registryName}/devices/${deviceId}`,
		versionToUpdate: version,
		binaryData: binaryData
	  };

	  client.projects.locations.registries.devices.modifyCloudToDeviceConfig(request,
		(err, data) => {
		  if (err) {
				errorCallback(err);
		  } else {
				successCallback(data);
		  }
		});
	  // [END iot_set_device_config]
	},

	// Retrieve the given device from the registry.
	getRegistry (client, registryId, projectId, cloudRegion) {
	  // [START iot_get_registry]
	  // Client retrieved in callback
	  // getClient(serviceAccountJson, function(client) {...});
	  // const cloudRegion = 'us-central1';
	  // const projectId = 'adjective-noun-123';
	  // const registryId = 'my-registry';
	  const parentName = `projects/${projectId}/locations/${cloudRegion}`;
	  const registryName = `${parentName}/registries/${registryId}`;
	  const request = {
		name: `${registryName}`
	  };

	  client.projects.locations.registries.get(request, (err, data) => {
		if (err) {
		  console.log('Could not find registry:', registryId);
		  console.log(err);
		} else {
		  console.log('Found registry:', registryId);
		  console.log(data.data);
		}
	  });
	  // [END iot_get_registry]
	},

	// Returns an authorized API client by discovering the Cloud IoT Core API with
	// the provided API key.
	getClient (serviceAccountJson, cb) {
			google.auth.getClient({
			scopes: ['https://www.googleapis.com/auth/cloud-platform']
			}).then(authClient => {
			const discoveryUrl =
				`${DISCOVERY_API}?version=${API_VERSION}`;

			google.options({
				auth: authClient
			});

			google.discoverAPI(discoveryUrl).then((client) => {
				cb(client);
			}).catch((err) => {
				cb(err);
				console.log('Error during API discovery.', err);
			});
			});
	},

	// Retrieves the IAM policy for a given registry.
	getIamPolicy (client, registryId, projectId, cloudRegion) {
	  // [START iot_get_iam_policy]
	  // Client retrieved in callback
	  // getClient(serviceAccountJson, function(client) {...});
	  // const cloudRegion = 'us-central1';
	  // const projectId = 'adjective-noun-123';
	  // const registryId = 'my-registry';
	  const parentName = `projects/${projectId}/locations/${cloudRegion}`;
	  const registryName = `${parentName}/registries/${registryId}`;
	  const request = {
		'resource_': `${registryName}`
	  };

	  client.projects.locations.registries.getIamPolicy(request, (err, data) => {
		if (err) {
		  console.log('Could not find policy for: ', registryId);
		  console.log('Trace: ', err);
		} else {
		  data = data.data;
		  console.log(`ETAG: ${data.etag}`);
		  data.bindings = data.bindings || [];
		  data.bindings.forEach((_binding) => {
			console.log(`Role: ${_binding.role}`);
			_binding.members || (_binding.members = []);
			_binding.members.forEach((_member) => {
			  console.log(`\t${_member}`);
			});
		  });
		}
	  });
	  // [END iot_get_iam_policy]
	},

	// Sets the IAM permissions for a given registry to a single member / role.
	setIamPolicy (
	  client,
	  registryId,
	  projectId,
	  cloudRegion,
	  member,
	  role
	) {
	  // [START iot_set_iam_policy]
	  // Client retrieved in callback
	  // setClient(serviceAccountJson, function(client) {...});
	  // const cloudRegion = 'us-central1';
	  // const projectId = 'adjective-noun-123';
	  // const registryId = 'my-registry';
	  const parentName = `projects/${projectId}/locations/${cloudRegion}`;
	  const registryName = `${parentName}/registries/${registryId}`;
	  const request = {
		'resource_': `${registryName}`,
		'resource': {'policy': {
		  'bindings': [{
			'members': member,
			'role': role
		  }]
		}}
	  };

	  client.projects.locations.registries.setIamPolicy(request, (err, data) => {
		if (err) {
		  console.log('Could not set policy for: ', registryId);
		  console.log('Trace: ', err);
		} else {
		  console.log(`ETAG: ${data.etag}`);
		  console.log(JSON.stringify(data));
		  data.bindings = data.bindings || [];
		  data.bindings.forEach((_binding) => {
			console.log(`Role: ${_binding.role}`);
			_binding.members || (_binding.members = []);
			_binding.members.forEach((_member) => {
			  console.log(`\t${_member}`);
			});
		  });
		}
	  });
	  // [END iot_set_iam_policy]
	}
}
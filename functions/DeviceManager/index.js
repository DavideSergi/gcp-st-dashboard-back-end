// !Important - TODO: to implement logic for AuthZ 

const Datastore = require('@google-cloud/datastore');
const admin = require('firebase-admin');
const cors = require('cors')({origin: true});
const express = require('express');
const HubHandle = require('./GCPCoreIoT.js');
const NodeRSA = require('node-rsa')

const app = express();
const registryId = "gcp-dashboard"
const projectId = "stm32iot-206219"
const cloudRegion = "europe-west1"
const databaseURL = 'https://stm32iot-206219.firebaseio.com'
const kind_UserDeviceRegistration = "UserDeviceRegistration"
const kind_UnclaimedDevices = "UnclaimedDevices" 
// Express middleware that validates Firebase ID Tokens passed in the Authorization HTTP header.
// The Firebase ID token needs to be passed as a Bearer token in the Authorization HTTP header like this:
// `Authorization: Bearer <Firebase ID Token>`.
// when decoded successfully, the ID Token content will be added as `req.user`.
const validateFirebaseIdToken = (req, res, next) => {

    //Doesn't require Authorization header for CORS handshaking
    if(req.method==='OPTIONS'){
        return next();
    }

    console.log('Check if request is authorized with Firebase ID token');
  
    if ((!req.headers.authorization || !req.headers.authorization.startsWith('Bearer '))) {
      console.error('No Firebase ID token was passed as a Bearer token in the Authorization header.',
          'Make sure you authorize your request by providing the following HTTP header:',
          'Authorization: Bearer <Firebase ID Token>');
      res.status(403).send('Unauthorized');
      return;
    }
  
    let idToken;
    if (req.headers.authorization && req.headers.authorization.startsWith('Bearer ')) {
      console.log('Found "Authorization" header');
      // Read the ID Token from the Authorization header.
      idToken = req.headers.authorization.split('Bearer ')[1];
    } else {
      res.status(403).send('Unauthorized');
      return;
    }
    admin.auth().verifyIdToken(idToken).then((decodedIdToken) => {
      console.log('ID Token correctly decoded', decodedIdToken);
      req.user = decodedIdToken;
      return next();
    }).catch((error) => {
      console.error('Error while verifying Firebase ID token:', error);
      res.status(403).send('Unauthorized');
    });
  };
// \ End validation middlware

// Initialize Admin Firebase handler
admin.initializeApp({
    credential: admin.credential.applicationDefault(),
    databaseURL: databaseURL
});

// Registrer a device for a authenticated user
app.post('/registerDevice', (req, res) => {
    /* Local DEBUG CORS handling*/
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "*");
    /* \ Local DEBUG CORS handling*/

    // validateFirebaseIdToken(req, res, function(){
    // ....
    // });
    // If it is called, so the user have sent a valid auth token
    // Private key of service account to generate an authorized client 
    // Extract public key fromb body
    var deviceId = req.body.deviceId;
    var publicKey = req.body.publicKey;
    var privateKey = req.body.privateKey;
    var userId = req.user.uid;

    var getClientCallback = function(client){
        var successCallback = function(data){

            // Insert in DB user device registration
            // Creates a client
            const datastore = new Datastore({
                projectId: projectId,
            });
            const transaction = datastore.transaction();
            // The name/ID for the new entity

            /* Task for UserDeviceRegistration kind */
            // The Cloud Datastore key for the new entity
            const taskKey_UserDeviceRegistration = datastore.key([kind_UserDeviceRegistration]);
            const processedmessage_UserDeviceRegistration = {
                userId:userId,
                deviceId:deviceId,
                timestamp: new Date((new Date()).toISOString())
            };
            // Prepares the new entity
            const task_UserDeviceRegistration = {
                key: taskKey_UserDeviceRegistration,
                data: processedmessage_UserDeviceRegistration,
            };


            /* Task for UnclaimedDevices kind */
            // The Cloud Datastore key for the new entity
            const taskKey_UnclaimedDevices = datastore.key([kind_UnclaimedDevices]);
            const processedmessage_UnclaimedDevices = {
                deviceId:deviceId,
                timestamp: new Date((new Date()).toISOString())
            };
            // Prepares the new entity
            const task_UnclaimedDevices = {
                key: taskKey_UnclaimedDevices,
                data: processedmessage_UnclaimedDevices,
            };            

            // Start transaction to save entities
            transaction
                .run()
                .then(
                    results => {
                        transaction.save([
                            task_UserDeviceRegistration,
                            task_UnclaimedDevices
                        ]);
                        transaction.commit();
                        res.status(200).send(userId);
                    }
                )
                .catch(() =>{ 
                    transaction.rollback();
                    var successCallback = function(){
                        res.status(500).send();
                    }.bind(this)
                    var errorCallback = function(){
                        res.status(500).send();
                    }.bind(this)
                    HubHandle.deleteDevice(
                        client,
                        deviceId,
                        registryId,
                        projectId,
                        cloudRegion,
                        successCallback,
                        errorCallback                    
                    )
                })

            // // Saves the entity
            // datastore
            //     .save(task_UserDeviceRegistration)
            //     .then(() => {
            //         console.log(`Saved successfully`);
            //         res.status(200).send(userId);
            //     })
            //     .catch(err => {
            //         console.error('ERROR:', err);
            //         res.status(500).send();
            //     });
        }
        var errorCallback = function(data){
            res.status(500).send();
        }

        // Create here private key and public key
        // ..
        // const key = new NodeRSA({b: 2048});
        // var publicKey = key.exportKey('pkcs8-public-pem');
        // var privateKey = key.exportKey('pkcs8-private-pem');
        var connectionString = "project-id=stm32iot-206219;registry-id=gcp-dashboard;device-id="+deviceId+";cloud-region=europe-west1"
        const metadata = {
            privateKey:privateKey,
            connectionString:connectionString
        }
        HubHandle.createRsaDevice(
            client,
            deviceId,
            registryId,
            projectId,
            cloudRegion,
            publicKey,
            metadata,
            successCallback,
            errorCallback
        );
    };
        
    HubHandle.getClient({},getClientCallback);
    return; 
})

// Check existing device identity
app.get("/checkExist",(req,res)=>{
    /* Local DEBUG CORS handling*/
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "*");
    /* \ Local DEBUG CORS handling*/

    var deviceId = req.query.deviceId;

    // Check existing device 
    var getClientCallback = function(client){
        var successCallback = function(dataDevice){
            res.status(200).send();
        }
        var errorCallback = function(errDataDevice){
            res.status(404).send();
        }
        HubHandle.getDevice(
            client,
            deviceId,
            registryId,
            projectId,
            cloudRegion,
            successCallback,
            errorCallback
        );           
    };
    HubHandle.getClient({},getClientCallback); 
});

// Delete existing device identity
app.get("/deleteDevice",(req,res)=>{
    /* Local DEBUG CORS handling*/
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "*");
    /* \ Local DEBUG CORS handling*/

    console.log("Start delete device function")
    
    var deviceId = req.query.deviceId;
    var userId = req.user.uid;
    const datastore = new Datastore({
        projectId: projectId,
    });
    query = datastore
        .createQuery(kind_UserDeviceRegistration)
        .filter('userId','=',userId)
        .filter('deviceId','=',deviceId);

    // Log
    console.log("Datastore client and query ready");
    console.log(deviceId+" - "+userId);

    datastore
        .runQuery(query)
        .then(
            result => {

                console.log(result[0]);

                var deviceUserRegistrations = result[0];
                var len = deviceUserRegistrations;
                var exited = false;

                // !Important - Hypotesis: one iteration
                deviceUserRegistrations.forEach(
                    function(deviceUserRegistration){
                        if(exited===true)
                            return ;
                        exited=true;

                        
                        // Starting deleting from datastore 
                        const registrationKey = deviceUserRegistration[datastore.KEY]

                        datastore.delete(
                            registrationKey,
                            function(err, apiResp) {
                                console.log(apiResp)
                                if(err){
                                    // Deleting from datastore failed
                                    console.log(err)
                                    res.status(500).send();
                                    return;
                                }
                                
                                // Deleting from datastore successfull
                                // Start deleteing existing device identity from hub
                                var getClientCallback = function(client){
                                    var successCallback = function(dataDevice){
                                        
                                        res.status(200).send();
                                        
                                    }
                                    var errorCallback = function(errDataDevice){
                                        
                                        res.status(500).send();

                                    }

                                    HubHandle.deleteDevice(
                                        client,
                                        deviceUserRegistration.deviceId,
                                        registryId,
                                        projectId,
                                        cloudRegion,
                                        successCallback,
                                        errorCallback
                                    );           
                                };
                                HubHandle.getClient({},getClientCallback); 
                            }
                        );
                    }
                );
            })
        .catch(
            result => {
                res.status(500).send();
            });

});

// Get given device already registered for authenticated user
app.get('/getDevice', (req, res) => {
    /* Local DEBUG CORS handling*/
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "*");
    /* \ Local DEBUG CORS handling*/
    
    // Get existing device 
    var deviceId = req.query.deviceId;

    var getClientCallback = function(client){
        var successCallback = function(dataDevice){
            console.log("# Data device");
            console.log(dataDevice);
            var successCallback = function(dataDeviceState){
                    console.log("# Data device state");
                    console.log(dataDeviceState);
                    var devicestate = null;
                    for(var i in dataDeviceState.deviceStates)
                        devicestate = dataDeviceState.deviceStates[i];

                    var aggregatedDevice = {
                        id: dataDevice.id,
                        lastHeartbeatTime: dataDevice.lastHeartbeatTime,
                        lastEventTime: dataDevice.lastEventTime,
                        binaryState: (devicestate!=null)?devicestate.binaryData:null,
                        lastSeenTime: (dataDevice.config.deviceAckTime)?dataDevice.config.deviceAckTime:null,
                        privateKey: (dataDevice.metadata)?dataDevice.metadata.privateKey:null,
                        connectionString: (dataDevice.metadata)?dataDevice.metadata.connectionString:null
                    }
                    res.status(200).send(aggregatedDevice);
                }
            var errorCallback = function(errDataDeviceState){
                    res.status(400).send(errDataDeviceState);
            };
            HubHandle.getDeviceState(
                client,
                deviceId,
                registryId,
                projectId,
                cloudRegion,
                successCallback,
                errorCallback
            );  
        }
        var errorCallback = function(errDataDevice){
            res.status(400).send(errDataDevice);
        }
        HubHandle.getDevice(
            client,
            deviceId,
            registryId,
            projectId,
            cloudRegion,
            successCallback,
            errorCallback
        );           
    };
    HubHandle.getClient({},getClientCallback);  
});

// Get devices already registered for authenticated user
app.get('/getDevices', (req, res) => {
    /* Local DEBUG CORS handling*/
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "*");
    /* \ Local DEBUG CORS handling*/

    // Retrieve devices from DB for authenticated user
    // Creates a client
    var userId = req.user.uid;

    const datastore = new Datastore({
        projectId: projectId,
    });

    query = datastore
        .createQuery(kind_UserDeviceRegistration)
        .filter('userId','=',userId)

    // Log
    console.log("Datastore client and query ready");
    
    datastore
        .runQuery(query)
        .then(
            result => {
                
                // Log
                console.log("Query result:");
                // console.log(result);

                deviceUserRegistrations = result[0];
                var registrationCount = deviceUserRegistrations.length;
                var devices = [];

                // Retrieve devices state from Core IoT 
                if(registrationCount==0){
                    res.status(200).send(devices);
                    return;
                }
                for(var index in deviceUserRegistrations){
                    
                    var deviceId = deviceUserRegistrations[index].deviceId;
                    var p = GetDeviceFromCoreIoT(deviceId);
                    p.then(
                        function(device){
                            // TODO: insert information about registration (timestamp, type-not available-, ...)
                            
                            devices.push(device);
                            registrationCount--;
                            if(registrationCount===0){
                                console.log(devices)
                                res.status(200).send(devices);
                            }
                        }
                    ).catch(
                        function(err){
                            registrationCount=0;
                            console.log(err)
                            res.status(500).send();
                        }
                    );
                }
            }
        ).catch(
            result =>{
                res.status(400).send(result);
            }
        );   
});

// Update given device for authenticated user
app.put('/updateDeviceState', (req, res) => {

    /* Local DEBUG CORS handling*/
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "*");
    /* \ Local DEBUG CORS handling*/

    // Check if device is registered for authenticated user 
    // Creates query
    // Retrieve devices from DB for authenticated user
    // Creates a client
    const datastore = new Datastore({
        projectId: projectId,
    });
    var deviceId = req.body.deviceId;
    var state = req.body.state;
    var userId = req.user.uid;
    
    var query = null;
    query = datastore
        .createQuery(kind_UserDeviceRegistration)
        .filter('deviceId','=',deviceId)
        .filter('userId','=',userId)

    // Perform query
    datastore
        .runQuery(query)
        .then(
            result => {
                devices = result[0];
                if(devices.length<=0)
                    res.status(403).send({message:"You are not admin of '"+deviceId+"}'"});
                // Update device state
                // Extract public key fromb body
                var getClientCallback = function(client){
                    var successCallback = function(data){
                        res.status(200).send();
                    }
                    var errorCallback = function(err){
                        console.log(err)
                        res.status(500).send(err);
                    }
                    var stateStr = JSON.stringify(state);
                    console.log(stateStr);
                    HubHandle.setDeviceConfig(
                        client,
                        deviceId,
                        registryId,
                        projectId,
                        cloudRegion,
                        stateStr,
                        0,
                        successCallback,
                        errorCallback
                    );
                };
                
                HubHandle.getClient({},getClientCallback);
            }
        ).catch(
            result =>{
                res.status(500).send(result);
            }
        );
    // Update device
});


// User manager function
//app.use(validateFirebaseIdToken);  // (w/ authN header [ Authorization  |  Bearer <token> ])
app.use(cors);
app.options('*', cors); 
exports.DeviceManager = (req, res) => {
    validateFirebaseIdToken(req,res,function(){
        app(req,res);
    }.bind(this))
};


function GetDeviceFromCoreIoT(deviceId){
    
    var p = new Promise(
        function(resolve, reject) {
            // Get existing device 
            var getClientCallback = function(client){
                var successCallback = function(dataDevice){
                    var successCallback = function(dataDeviceState){
                        var devicestate = null;
                        for(var i in dataDeviceState.deviceStates){
                            devicestate = dataDeviceState.deviceStates[i];
                            break;
                        }
                        var aggregatedDevice = {
                            id: dataDevice.id,
                            lastHeartbeatTime: dataDevice.lastHeartbeatTime,
                            lastEventTime: dataDevice.lastEventTime,
                            binaryState: (devicestate!=null)?devicestate.binaryData:null,
                            lastSeenTime: (dataDevice.config.deviceAckTime)?dataDevice.config.deviceAckTime:null,
                            privateKey:(dataDevice.metadata)?dataDevice.metadata.privateKey:null,
                            connectionString:(dataDevice.metadata)?dataDevice.metadata.connectionString:null
                            // device : dataDevice,
                            // deviceState : dataDeviceState
                        }
                        resolve(aggregatedDevice);
                        //res.status(200).send(aggregatedDevice);
                    }
                    var errorCallback = function(errDataDeviceState){
                        reject(errDataDeviceState);
                        //res.status(400).send(errDataDeviceState);
                    }
                    HubHandle.getDeviceState(
                        client,
                        deviceId,
                        registryId,
                        projectId,
                        cloudRegion,
                        successCallback,
                        errorCallback
                    );  
                }
                var errorCallback = function(errDataDevice){
                    reject(errDataDevice);
                    //res.status(400).send(errDataDevice);
                }
                HubHandle.getDevice(
                    client,
                    deviceId,
                    registryId,
                    projectId,
                    cloudRegion,
                    successCallback,
                    errorCallback
                );           
            };
            HubHandle.getClient({},getClientCallback);   
        });
    return p;
}

/**
 * Old version: without express framework
 * 
*/
// exports.RegistrationFunction = (req,res)=>{
//     res.set('Access-Control-Allow-Origin', "*");
//     res.set('Access-Control-Allow-Methods', 'GET, POST, PUT');
//     res.set("Access-Control-Allow-Headers", "Content-Type");
//     res.set("Access-Control-Max-Age", "3600");
    
//     // OPTION: CORS handling
//     if(req.method==='OPTIONS'){
//         console.log("OPTIONS method received");
//         res.status(200).send();
//         return;
//     }
//     // POST: Create device
//     if(req.method === 'POST')
//     {
//         // Private key of service account to generate an authorized client 
//         // Extract public key fromb body
//         var deviceId = req.body.deviceId;
//         var publicKey = req.body.publicKey;

//         var getClientCallback = function(client){
//             var successCallback = function(data){
//                 res.status(200).send();
//             }
//             var errorCallback = function(data){
//                 res.status(400).send();
//             }
//             HubHandle.createRsaDevice(
//                 client,
//                 deviceId,
//                 registryId,
//                 projectId,
//                 cloudRegion,
//                 publicKey,
//                 successCallback,
//                 errorCallback
//             );
//         };
        
//         HubHandle.getClient({},getClientCallback);
//         return;
//     }
//     // GET: Get existing device
//     if(req.method === 'GET'){
//         var existing = req.query.exist;
//         var deviceId = req.query.deviceId;

//         if(deviceId === null || deviceId === undefined || deviceId === "")
//         {
//             console.log("DeviceId parameter is missing.");
//             res.status(400).send(data);
//             return;
//         }


//         if(!existing){
//             // Get existing device 
//             var getClientCallback = function(client){
//                 var successCallback = function(dataDevice){
//                     var successCallback = function(dataDeviceState){
//                         console.log(dataDeviceState);
//                         // TODO: check
//                         var devicestate = null;
//                         for(var i in dataDeviceState.deviceStates)
//                             devicestate = dataDeviceState.deviceStates[i];

//                         var aggregatedDevice = {
//                             id: dataDevice.id,
//                             lastHeartbeatTime: dataDevice.lastHeartbeatTime,
//                             binaryState: (devicestate!=null)?devicestate.binaryData:null,
//                             lastSeenTime: (dataDevice.config.deviceAckTime)?dataDevice.config.deviceAckTime:null
//                             // device : dataDevice,
//                             // deviceState : dataDeviceState
//                         }
//                         res.status(200).send(aggregatedDevice);
//                     }
//                     var errorCallback = function(errDataDeviceState){
//                         res.status(400).send(errDataDeviceState);
//                     }
//                     HubHandle.getDeviceState(
//                         client,
//                         deviceId,
//                         registryId,
//                         projectId,
//                         cloudRegion,
//                         successCallback,
//                         errorCallback
//                     );  
//                 }
//                 var errorCallback = function(errDataDevice){
//                     res.status(400).send(errDataDevice);
//                 }
//                 HubHandle.getDevice(
//                     client,
//                     deviceId,
//                     registryId,
//                     projectId,
//                     cloudRegion,
//                     successCallback,
//                     errorCallback
//                 );           
//             };
//             HubHandle.getClient({},getClientCallback);    
//         }else{
//             // Check existing device 
//             var getClientCallback = function(client){
//                 var successCallback = function(dataDevice){
//                     res.status(200).send();
//                 }
//                 var errorCallback = function(errDataDevice){
//                     res.status(404).send(errDataDevice);
//                 }
//                 HubHandle.getDevice(
//                     client,
//                     deviceId,
//                     registryId,
//                     projectId,
//                     cloudRegion,
//                     successCallback,
//                     errorCallback
//                 );           
//             };
//             HubHandle.getClient({},getClientCallback); 
//         }
//     }
//     // PUT: Update device state
//     if(req.method === 'PUT')
//     {
//         // Private key of service account to generate an authorized client 
//         // Extract public key fromb body
//         var deviceId = req.body.deviceId;
//         var state = req.body.state;

//         var getClientCallback = function(client){
//             var successCallback = function(data){
//                 res.status(200).send();
//             }
//             var errorCallback = function(data){
//                 console.log(data)
//                 res.status(400).send();
//             }
//             var stateStr = JSON.stringify(state);
//             console.log(stateStr);
//             HubHandle.setDeviceConfig(
//                 client,
//                 deviceId,
//                 registryId,
//                 projectId,
//                 cloudRegion,
//                 stateStr,
//                 0,
//                 successCallback,
//                 errorCallback
//             );
//         };
        
//         HubHandle.getClient({},getClientCallback);
//         return;
//     }


// }




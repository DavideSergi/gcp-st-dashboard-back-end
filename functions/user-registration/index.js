
const admin = require('firebase-admin');
const firebase = require('firebase');
const cors = require('cors')({origin: false});
const express = require('express');
const app = express();

// Express middleware that validates Firebase ID Tokens passed in the Authorization HTTP header.
// The Firebase ID token needs to be passed as a Bearer token in the Authorization HTTP header like this:
// `Authorization: Bearer <Firebase ID Token>`.
// when decoded successfully, the ID Token content will be added as `req.user`.
const validateFirebaseIdToken = (req, res, next) => {
  console.log('Check if request is authorized with Firebase ID token');

  if ((!req.headers.authorization || !req.headers.authorization.startsWith('Bearer '))) {
    console.error('No Firebase ID token was passed as a Bearer token in the Authorization header.',
        'Make sure you authorize your request by providing the following HTTP header:',
        'Authorization: Bearer <Firebase ID Token>');
    res.status(403).send('Unauthorized');
    return;
  }

  let idToken;
  if (req.headers.authorization && req.headers.authorization.startsWith('Bearer ')) {
    console.log('Found "Authorization" header');
    // Read the ID Token from the Authorization header.
    idToken = req.headers.authorization.split('Bearer ')[1];
  } else {
    res.status(403).send('Unauthorized');
    return;
  }
  admin.auth().verifyIdToken(idToken).then((decodedIdToken) => {
    console.log('ID Token correctly decoded', decodedIdToken);
    req.user = decodedIdToken;
    return next();
  }).catch((error) => {
    console.error('Error while verifying Firebase ID token:', error);
    res.status(403).send('Unauthorized');
  });
};
// \ End validation middlware

// Initialize Admin Firebase handler
admin.initializeApp({
    credential: admin.credential.applicationDefault(),
    databaseURL: 'https://stm32iot-206219.firebaseio.com'
});
// Initialize Firebase handler
firebase.initializeApp({
  apiKey: "AIzaSyCIbfpaLZKaGWDcAeKDOQyALWlJBgly2wU",
  authDomain: "stm32iot-206219.firebaseapp.com",
  databaseURL: "https://stm32iot-206219.firebaseio.com",
  projectId: "stm32iot-206219",
  storageBucket: "stm32iot-206219.appspot.com",
  messagingSenderId: "598690661873"
});


/* REST endpoints */
// Create user enpoint (w/o AuthN header)
app.post('/createUser', (req, res) => {
  
  var email = req.body.email;
  var password = req.body.password;
  var name = req.body.name;

  admin.auth().createUser({
    email: email,
    password: password,
    displayName: name,
    disabled: false
  })
    .then(function(userRecord) {
      // See the UserRecord reference doc for the contents of userRecord.
      console.log("Successfully created new user:", userRecord.uid);
      res.status(200).send(userRecord);
    })
    .catch(function(error) {
      console.log("Error creating new user:", error);
      res.status(400).send(error);
    });
});
// Login user enpoint (w/ authN header [ Authorization  |  Bearer <token> ])
app.get('/getUser', (req, res) => {
  validateFirebaseIdToken(req, res, function(){
    // If it is called, so the user have sent a valid auth token
    res.writeHead(200, {"Content-Type": "application/json"});
    res.end(JSON.stringify(req.user));// req.user set by validateFirebaseIdToken 
  });
})

// Create user enpoint (w/o AuthN header)
app.post('/login', (req, res) => {
  if(req===undefined||res===undefined) return;
  var email = req.body.email;
  var password = req.body.password;

  // Sign-in
  firebase.auth().signInWithEmailAndPassword(email, password)
  .then(function(user){
    // Get valid authN token for user just logged in
    firebase.auth().currentUser.getIdToken().then((token)=>{
    //user.getIdToken(true).then((token)=>{
        res.writeHead(200, {"Content-Type": "application/json"});
        res.end(JSON.stringify({token:token}));
    }).catch((err)=>{
        res.writeHead(403, {"Content-Type": "application/json"});
        res.end(JSON.stringify({error:err}));
    });
  })
  .catch(function(error) {
    res.status(403).send("Unauthorized : "+error); // FIXME to remove error in production case
  });
})

// Logout user enpoint (w/ authN header [ Authorization  |  Bearer <token> ])
app.get('/logout', (req, res) => {
  validateFirebaseIdToken(req, res, function(){
    // If it is called, so the user have sent a valid auth token

  });
})

// User manager function
app.use(cors);
exports.UserManager = (req, res) => {
  app(req,res);
};
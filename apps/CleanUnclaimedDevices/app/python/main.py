
import cgi
import textwrap
import urllib
from datetime import datetime, timedelta

import webapp2
from google.appengine.ext import ndb
from googleapiclient import discovery
from google.auth import app_engine

project_id = "stm32iot-206219"
cloud_region = "europe-west1"
registry_id = "gcp-dashboard"

# [START get_client]
def get_client():
    api_scopes =['https://www.googleapis.com/auth/sqlservice.admin']
    api_version = 'v1'
    discovery_api = 'https://cloudiot.googleapis.com/$discovery/rest'
    service_name = 'cloudiotcore'

    #credentials = app_engine.Credentials(scopes=api_scopes)

    discovery_url = '{}?version={}'.format(discovery_api, api_version)
    
    return discovery.build(
                service_name,
                api_version,
                discoveryServiceUrl=discovery_url,
                )#credentials=credentials)
# [END get_client]

# [START iot_get_device]
def iot_get_device(client,device_id):
    registry_name = 'projects/{}/locations/{}/registries/{}'.format(project_id, cloud_region, registry_id)
    device_name = '{}/devices/{}'.format(registry_name, device_id)
    devices = client.projects().locations().registries().devices()
    device = devices.get(name=device_name).execute()
    return device
# [END iot_get_device]

# # [START iot_get_device_state]
# def iot_get_device_state(client,device_id):
#     registry_name = 'projects/{}/locations/{}/registries/{}'.format(project_id, cloud_region, registry_id)
#     device_name = '{}/devices/{}'.format(registry_name, device_id)
#     states = client.projects().locations().registries().devices().states()
#     deviceState = states.list(name=device_name).execute()
#     print("Raw device state:")
#     print(deviceState)
#     return deviceState[0]
# # [END iot_get_device_state]

# [START iot_delete_device]
def iot_core_delete_device(client,device_id):
    """Delete the device with the given id."""
    print(client)
    registry_name = 'projects/{}/locations/{}/registries/{}'.format(project_id, cloud_region, registry_id)
    device_name = '{}/devices/{}'.format(registry_name, device_id)
    devices = client.projects().locations().registries().devices()
    return devices.delete(name=device_name).execute()
# [END iot_delete_device]

# [START UnclaimedDevices]
class UnclaimedDevices(ndb.Model):
    """Models an individual UnclaimedDevices entry with content and date."""
    deviceId = ndb.StringProperty()
    timestamp = ndb.DateTimeProperty()
    # need to map all other properties?

# [START query]
    @classmethod
    def query_old_unclaimed_devices(cls):
        return cls.query(cls.timestamp<=(datetime.now() - timedelta(minutes=10))) 
# [END query]
# [END UnclaimedDevices]

# [START UserDeviceRegistration]
class UserDeviceRegistration(ndb.Model):
    """Models an individual UserDeviceRegistration entry with content and date."""
    deviceId = ndb.StringProperty()
    userId = ndb.StringProperty()
    timestamp = ndb.DateTimeProperty()
    # need to map all other properties?

# [START query]
    @classmethod
    def query_user_device_registration_by_device_id(cls,deviceId):
        return cls.query(cls.deviceId == deviceId)
# [END query]
# [END UserDeviceRegistration]

class StartTask(webapp2.RequestHandler):
    # @ndb.transactional
    def get(self):
        # Build google client iotcore
        client = get_client()
        # Perform query
        devices = UnclaimedDevices.query_old_unclaimed_devices().fetch()
        # Log
        print("Device unclaimed more than 10 minutes:")
        print(devices)

        for device in devices:
            try:
                hubDevice = iot_get_device(client,device.deviceId)
                print("Device from Core IoT:")
                print(hubDevice)
                if not 'lastHeartbeatTime' in hubDevice:
                    print("Device will be removed: "+device.deviceId)
                    userDeviceRegistrations = UserDeviceRegistration.query_user_device_registration_by_device_id(device.deviceId).fetch(keys_only=True)
                    ndb.delete_multi(userDeviceRegistrations)
                    iot_core_delete_device(client,device.deviceId)
                # In any case because can be has been claimed
                device.key.delete()
            except:
                print("An exception occurred")

        return self.response.out.write(devices)



app = webapp2.WSGIApplication([
    ('/*', StartTask),
], debug=True)
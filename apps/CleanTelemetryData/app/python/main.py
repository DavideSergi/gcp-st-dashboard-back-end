
import cgi
import textwrap
import urllib
from datetime import datetime, timedelta

import webapp2
from google.appengine.ext import ndb


# [START telemetry data]
class Telemetry(ndb.Model):
    """Models an individual Telemetry entry with content and date."""
    id = ndb.StringProperty()
    timestamp = ndb.DateTimeProperty()
    # need to map all other properties?
# [END telemetry data]

# [START query]
    @classmethod
    def query_old_telemetry_data(cls):
        return cls.query(cls.timestamp<=(datetime.now() - timedelta(hours=12)))
# [END query]


class StartTask(webapp2.RequestHandler):
    def get(self):
        # Perform query
        data = Telemetry.query_old_telemetry_data().fetch(keys_only=True)
        # Log
        print(data)
        # Store backup telemetry entity
        try:
            backup = Telemetry(id='backup____entity',timestamp=datetime.now())
            backup.put()
        except:
            print "Backup entity already exist"

        # Delete selected data
        ndb.delete_multi(data)

        return self.response.out.write(data)


app = webapp2.WSGIApplication([
    ('/*', StartTask),
], debug=True)